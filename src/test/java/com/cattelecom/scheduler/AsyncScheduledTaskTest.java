package com.cattelecom.scheduler;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

public class AsyncScheduledTaskTest {

	private static final Logger log = LoggerFactory.getLogger(AsyncScheduledTaskTest.class);

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Async
	@Scheduled(cron = "0 */1 * * * ?")
	public void runTask() {
		log.info("This tested task is run on {}", dateFormat.format(new Date()));
		
        // Get time to sleep 
        long timeToSleep = 90L; 
		
        sleep(timeToSleep);
		
		log.info("This tested task is finished on {}", dateFormat.format(new Date()));
	}
	
	private void sleep(long seconds) {		  
        // Create a TimeUnit object 
        TimeUnit time = TimeUnit.SECONDS; 
		
		try {
			
			time.sleep(seconds);
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
