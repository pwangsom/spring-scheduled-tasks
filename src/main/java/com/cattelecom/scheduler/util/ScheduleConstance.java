package com.cattelecom.scheduler.util;

public class ScheduleConstance {
	public final static  String LOG_STATUS_NEW="new";
	public final static String LOG_STATUS_INPROGRESS="in-progress";	
	public final static String LOG_STATUS_QUEUE="queue";
	public final static String CA_UPDATE_INTERFACE_TYPE="SYNCHRONIZE";
	public final static String CA_UPDATE_INTERFACE_CODE_CA_UPDATE= "CA-CHANGE";
	public final static String CA_UPDATE_INTERFACE_SOURCE_CA_UPDATE= "CRMINF";
 	public final static String CA_UPDATE_INTERFACE_BY_CA_UPDATE= "SCHEDULE-ADMIN";
 
 	//restful
	public final static String RESPONE_CODE_SUCCESS = "SUCCESS";
	public final static String RESPONE_CODE_FAIL = "FAIL";
	public final static String RESPONE_CODE_NOTMATCH = "NOTMATCH";
	public final static String RESPONE_CODE_DUPLICATE = "DUPLICATE";
	public final static String RESPONE_CODE_PARAMETER_INVALID = "BAD-REQUEST";
	public final static String RESPONE_CODE_NOTFOUND = "NOTFOUND";
}
