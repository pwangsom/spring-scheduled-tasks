package com.cattelecom.scheduler.rest;

import java.util.Date;

import com.cattelecom.scheduler.util.ScheduleConstance;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Getter;
import lombok.Setter;

 
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Getter
@Setter
@JsonDeserialize(as = ResponseContent.class)
public class ResponseContent {
	protected String responseCode;
	protected String responseMessage;
	protected String cdbTransactionId;
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	protected Date responseDate;

	 

 

}
