package com.cattelecom.scheduler.rest.response;

import com.cattelecom.scheduler.rest.ResponseContent;
import com.cattelecom.scheduler.rest.response.model.ResponseItem;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
//import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonDeserialize(as = SynchronizeResponse.class)
public class SynchronizeResponse extends ResponseContent {

	private String interfaceId;
	private ResponseItem responseItems;
 

}
