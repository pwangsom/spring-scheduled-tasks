package com.cattelecom.scheduler.service;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

@Service
public class RestfulService {

 	public HttpHeaders setBasicAuthHeader(Map<String,String> valueList,String username,String password) {
		HttpHeaders header = new HttpHeaders();
		header .setContentType(MediaType.APPLICATION_JSON);
		header.setAll(valueList);
		header.setAccessControlAllowCredentials(true);
		header.setBasicAuth(username, password, StandardCharsets.UTF_8);
		return header;
	}
 
}
