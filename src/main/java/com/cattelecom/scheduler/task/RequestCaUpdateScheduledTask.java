package com.cattelecom.scheduler.task;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.cattelecom.scheduler.rest.response.SynchronizeResponse;
import com.cattelecom.scheduler.service.RestfulService;
import com.cattelecom.scheduler.util.ScheduleConstance;

import lombok.extern.slf4j.Slf4j;

@Component
@EnableAsync
@Slf4j
public class RequestCaUpdateScheduledTask {
	//@Autowired
	//RestTemplate restTemplate;
	@Autowired
	RestfulService restService;
	@Value("${endpoint.ca.update.url}")
	String caUpdateUrl;
	@Value("${endpoint.ca.update.user}")
	String caUpdateUsername;
	@Value("${endpoint.ca.update.password}")
	String caUpdatePassword;
	private static final SimpleDateFormat idFormat = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
	private static final SimpleDateFormat reqFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

	@Scheduled(cron = "0 0,10,20,30,40,50 * * * *") // every 1min
	public void executeTriggerCaUpdate() throws Exception {

		String interfaceId = idFormat.format(new Date());
		String requestDate = reqFormat.format(new Date());

		Map<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("interface_type", ScheduleConstance.CA_UPDATE_INTERFACE_TYPE);
		headerMap.put("interface_code", ScheduleConstance.CA_UPDATE_INTERFACE_CODE_CA_UPDATE);
		headerMap.put("interface_source", ScheduleConstance.CA_UPDATE_INTERFACE_SOURCE_CA_UPDATE);
		headerMap.put("interface_id", interfaceId);
		headerMap.put("request_date", requestDate);
		headerMap.put("request_by", ScheduleConstance.CA_UPDATE_INTERFACE_BY_CA_UPDATE);

		HttpHeaders headers = restService.setBasicAuthHeader(headerMap, caUpdateUsername, caUpdatePassword);

		HttpEntity<SynchronizeResponse> request = new HttpEntity<>(headers);

		ResponseEntity<SynchronizeResponse> response = new RestTemplate().exchange(caUpdateUrl, HttpMethod.GET, request,
				SynchronizeResponse.class);

		SynchronizeResponse resp = response.getBody();
		
		log.debug(interfaceId+"<*****>" + requestDate);
		if (null != resp.getInterfaceId())
			log.debug(interfaceId+"==>Response Id:" + resp.getInterfaceId());
		if (null != resp.getResponseCode())
			log.debug(interfaceId+"==>Response Code:" + resp.getResponseCode());
		if (null != resp.getResponseMessage())
			log.debug(interfaceId+"==>Response Msg:" + resp.getResponseMessage());
		 
		if (null != resp.getResponseItems().getItemCount())
			log.debug( interfaceId+"==>Request Customer Account Update total synchronize item:" + resp.getResponseItems().getItemCount());
		else
			log.debug( interfaceId+"==>Request Customer Account Update total synchronize item:0");

	}
}
